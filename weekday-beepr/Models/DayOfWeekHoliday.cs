﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr.Models
{
    public class DayOfWeekHoliday : IHoliday
    {
        public string Name { get; set; }
        public HolidayType Type { get { return HolidayType.DayOfMonth; } }
        public int Month { get; set; }
        public int Day { get; set; }
        public int? DayOfWeek { get; set; }
        private DateTime? _actualDate = null;

        public bool InDate(DateTime day)
        {
            if (_actualDate == null || _actualDate.Value.Year!=day.Year)
            {
                _actualDate = new DateTime(day.Year, Month, 1);
                // Get Difference between first day of the month vs day of the week that is needed
                int diff = (int)_actualDate.Value.DayOfWeek>DayOfWeek.Value? 
                                    (DayOfWeek.Value- (int)_actualDate.Value.DayOfWeek)+7: 
                                    (int)_actualDate.Value.DayOfWeek- DayOfWeek.Value;
                _actualDate = _actualDate.Value.AddDays(diff)
                                                .AddDays((Day-1)*7);// then add Days() * 7 to get how many *Mondays* eg. 2nd monday of june

            }
            return _actualDate == day;
        }
    }
}
