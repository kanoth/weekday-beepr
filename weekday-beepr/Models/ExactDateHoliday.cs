﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr.Models
{
    public class ExactDateHoliday:IHoliday
    {
        public string Name { get; set; }
        public HolidayType Type { get { return HolidayType.Exact; } }
        public int Month { get; set; }
        public int Day { get; set; }
        public int? DayOfWeek { get; set; }

        private DateTime? _actualDate = null;

        public bool InDate(DateTime day)
        {
            if(_actualDate== null || _actualDate.Value.Year != day.Year)
            {
                _actualDate = new DateTime(day.Year, Month, Day);
            }
            return day == _actualDate;
        }
    }
}
