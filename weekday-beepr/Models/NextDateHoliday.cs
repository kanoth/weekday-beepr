﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr.Models
{
    public class NextDateHoliday:IHoliday
    {
        public string Name { get; set; }
        public HolidayType Type { get { return HolidayType.Close; } }
        public int Month { get; set; }
        public int Day { get; set; }
        public int? DayOfWeek { get; set; }
        private DateTime? _actualDate = null;

        public bool InDate(DateTime day)
        {
            if (_actualDate == null || _actualDate.Value.Year != day.Year)
            {
                _actualDate = new DateTime(day.Year, Month, Day);
                // If Saturday add 2 days to actual date so it's monday or if sunday add 1
                if(_actualDate.Value.DayOfWeek == System.DayOfWeek.Saturday)
                {
                    _actualDate = _actualDate.Value.AddDays(2);
                }else if(_actualDate.Value.DayOfWeek == System.DayOfWeek.Sunday)
                {
                    _actualDate = _actualDate.Value.AddDays(1);
                }
            }
            return day == _actualDate;
        }
    }
}
