﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr
{
    public interface IHolidayService
    {
        public IReadOnlyCollection<IHoliday> Holidays { get; }

        public int NumberOfWeekDays(DateTime from, DateTime to);
    }
}
