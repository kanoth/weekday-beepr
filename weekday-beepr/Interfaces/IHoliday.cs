﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr
{
    public interface IHoliday
    {
        public string Name { get; set; }
        public HolidayType Type { get; }

        public int Month { get; set; }

        public int Day { get; set; }

        public int? DayOfWeek { get; set; }

        public bool InDate(DateTime day);

    }
}
