﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace weekday_beepr.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DayController : ControllerBase
    {

        private readonly ILogger<DayController> _logger;
        private IHolidayService _holidayService;

        public DayController(ILogger<DayController> logger,IHolidayService holidayService)
        {
            _logger = logger;
            _holidayService = holidayService;
        }
        [HttpGet]
        public int Get()
        {
            return 0;
        }
        [HttpGet]
        public int Get(DateTime fromDate, DateTime toDate)
        {
            return _holidayService.NumberOfWeekDays(fromDate,toDate);
        }
    }
}