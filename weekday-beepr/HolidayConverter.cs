﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using weekday_beepr.Models;

namespace weekday_beepr
{
    public class HolidayConverter : JsonConverter<IHoliday>
    {
        public override IHoliday ReadJson(JsonReader reader, Type objectType, [AllowNull] IHoliday existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;
            JObject obj = JObject.Load(reader);
            HolidayType holidayType = (HolidayType)(int)obj["Type"];
            switch (holidayType)
            {
                case HolidayType.Exact:
                    existingValue = new ExactDateHoliday
                                        {
                                            Day = (int)obj["Day"],
                                            Month = (int)obj["Month"],
                                            Name = obj["Name"].ToString()
                                        };
                    break;
                case HolidayType.Close:
                    existingValue = new NextDateHoliday
                    {
                        Day = (int)obj["Day"],
                        Month = (int)obj["Month"],
                        Name = obj["Name"].ToString()
                    };
                    break;
                case HolidayType.DayOfMonth:
                    existingValue = new DayOfWeekHoliday
                    {
                        Day = (int)obj["Day"],
                        Month = (int)obj["Month"],
                        Name = obj["Name"].ToString(),
                        DayOfWeek = (int)obj["DayOfWeek"]
                    };
                    break;
                default:
                    break;
            }
            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, [AllowNull] IHoliday value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
