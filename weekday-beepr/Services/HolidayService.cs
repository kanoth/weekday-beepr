﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr
{
    public class HolidayService: IHolidayService
    {
        private static List<IHoliday> holidays = new List<IHoliday>();

        private static readonly HolidayService configService = new HolidayService();
        private HolidayService()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            holidays = JsonConvert.DeserializeObject<List<IHoliday>>(File.ReadAllText(path + "Data\\holidays.json"), new HolidayConverter());
        }

        public static HolidayService GetInstance() => configService;

        // Calculate the number of week days between two dates, taking into account holidays
        public int NumberOfWeekDays(DateTime from, DateTime to)
        {
            TimeSpan p = to - from;
            int count = 0;
            for(var i = 0;i<p.TotalDays;i++)
            {
                DateTime loopingTime = from.AddDays(i);
                if (holidays.Any((x) => x.InDate(loopingTime))) continue;
                if(loopingTime.DayOfWeek!= DayOfWeek.Saturday || loopingTime.DayOfWeek != DayOfWeek.Sunday)
                {
                    count++;
                }
            }
            return count;
        }

        public IReadOnlyCollection<IHoliday> Holidays { get { return holidays.AsReadOnly(); } }
    }
}
