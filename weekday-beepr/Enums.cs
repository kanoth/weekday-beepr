﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace weekday_beepr
{
    public enum HolidayType
    {
        // Exact Holidays only
        Exact,
        // Exact Holiday unless on weekend
        Close,
        // Day of month eg. second Monday of Month
        DayOfMonth
    }
}
