using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Newtonsoft.Json;
using weekday_beepr;
using weekday_beepr.Models;
using System.IO;

namespace weekday_beepr.tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Setup()
        {
            List<IHoliday> holidays = new List<IHoliday>();
            holidays.Add(new ExactDateHoliday { Day = 1, Month = 1, Name = "New years day" });
            holidays.Add(new NextDateHoliday { Day = 26, Month = 1, Name = "Australia Day" });
            holidays.Add(new DayOfWeekHoliday { Day = 2, Month = 6, Name = "Queens Birthday", DayOfWeek = 1 });

            // serialize JSON to a string and then write string to a file
            File.WriteAllText(@"holidays.json", JsonConvert.SerializeObject(holidays));
            Assert.IsTrue(true);
        }
    }
}
