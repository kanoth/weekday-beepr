﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace weekday_beepr.tests
{
    [TestClass]
    public class ServiceTest
    {
        public static IHolidayService holidayService;
        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            //Initialise service
            holidayService = HolidayService.GetInstance();
        }

        [TestMethod]
        // Check to see if holiday service as holidays assigned
        public void HasHolidays()
        {
            Assert.IsTrue(holidayService.Holidays.Count > 0,"Invalid Holidays Assigned");
        }

        [TestMethod]
        public void InBetweenDates()
        {
            DateTime fromDate = new DateTime(2019, 12, 31);
            DateTime toDate = new DateTime(2020, 1, 3);
            Assert.IsTrue(holidayService.NumberOfWeekDays(fromDate, toDate) == 2,"Incorrect work days found");
        }
    }
}
