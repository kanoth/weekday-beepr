﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using weekday_beepr.Models;

namespace weekday_beepr.tests
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class HolidayTest
    {
        [TestMethod]
        public void DayOfWeekTest()
        {
            // Second Monday of June
            IHoliday holidayTest = new DayOfWeekHoliday { Name = "Queens birthday", Month = 6, Day = 2, DayOfWeek = 1 };
            DateTime queensBirthday = new DateTime(2021, 6, 14);
            Assert.IsTrue(holidayTest.InDate(queensBirthday),"Day of week (2021) fails");
            queensBirthday = new DateTime(2020, 6, 8);
            Assert.IsTrue(holidayTest.InDate(queensBirthday), "Day of week (2020) fails");
        }
        [TestMethod]
        public void ExactDayTest()
        {
            IHoliday holidayTest = new ExactDateHoliday { Name = "New Years Day", Month = 1, Day = 1};
            DateTime newYearsDay = new DateTime(2020, 1, 1);
            Assert.IsTrue(holidayTest.InDate(newYearsDay), "ExactDateHoliday fails");
        }
        [TestMethod]
        public void NextDateTest()
        {
            IHoliday holidayTest = new NextDateHoliday { Name = "Australia Day", Month = 1, Day = 26};
            DateTime australiaDay = new DateTime(2020, 1, 27);
            Assert.IsTrue(holidayTest.InDate(australiaDay), "NextDateHoliday fails");
        }
    }
}
